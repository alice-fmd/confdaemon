// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
//
// ROOT based acquisition 
//
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>
#include <vector>

//____________________________________________________________________
/** Function template to decode a string into a value. 
    @param str String to decode
    @param v On return, the value decoded
    @ingroup Utils
*/
template <typename T>
void str2val(const std::string& str, T& v) 
{
  std::stringstream s(str);
  if (str[0] == '0') {
    if (str[1] == 'x' || str[1] == 'X') s >> std::hex >> v;
    else                                s >> std::oct >> v;
  }
  else 
    s >> v;
}

//____________________________________________________________________
/** Function template to decode a string into a value. 
    @param str String to decode
    @param v On return, the value decoded
    @ingroup Utils
*/
template <typename T>
void str2val(const char* str, T& v)
{
  str2val<T>(std::string(str), v);
}


//____________________________________________________________________
/** @struct OptionBase Options.h <rcudata/Options.h>
    @brief Base class for options. 
    @ingroup Utils
 */
struct OptionBase
{
  /** Constructor 
      @param s Short option, if any
      @param l Long option, if any
      @param h Help string
      @param arg Whether this option needs an argument  */
  OptionBase(char s, const char* l, const char* h, bool arg)
    : fShort(s), 
      fLong(l), 
      fHelp(h), 
      fNeedArg(arg), 
      fSet(false)
  {}
  /** Destructor */
  virtual ~OptionBase() {}
  /** @return @c true if value is set. */
  bool IsSet() const { return fSet; }
  /** @return @c true if this option need an argument */
  bool NeedArg() const { return fNeedArg; }
  /** @return Short option character */
  virtual char Short() const { return fShort; }
  /** @return Long option string */
  virtual const std::string& Long() const { return fLong; }
  /** Handle an option. If the option matches, decode the value if
      needed and return @c true. 
      @param s Short option character
      @param arg argument to possibly decode to value
      @return @c true if we handled this option */
  virtual bool Handle(char s, const char* arg) = 0;
  /** Handle an option. If the option matches, decode the value if
      needed and return @c true. 
      @param l Long option string
      @param arg argument to possibly decode to value
      @return @c true if we handled this option */
  virtual bool Handle(const std::string& l, const std::string& arg) = 0;
  /** Print help information. */
  virtual void Help() const 
  {
    std::cout << "\t-" << fShort << ",--" << fLong 
	      << (fNeedArg ? "=ARG" : "") << "\t" << fHelp;
  }
protected:
  /** Short option character */
  char        fShort;
  /** Long option string */
  std::string fLong;
  /** Help string */
  std::string fHelp;
  /** Whether this option needs an argument */
  bool        fNeedArg;
  /** Whether the option has been set */
  bool        fSet;
};
  
namespace std 
{
  /** @struct less 
      @ingroup Utils
      @brief Function to compare options */
  template <>
  struct less<OptionBase*> 
  {
    /** function operator 
	@return @c true if @a lhs is less than @a rhs */
    bool operator()(OptionBase* const & lhs, OptionBase* const & rhs) 
    {
      if (lhs->Short() == '\0' && rhs->Short())
	return lhs->Long() < rhs->Long();
      return lhs->Short() < rhs->Short();
    }
  };
}

//____________________________________________________________________
/** Class template for command line options 
    @ingroup Utils
 */
template <typename T> 
struct Option : public OptionBase
{
  /** Option constructor 
      @param s Short option
      @param l Long option
      @param h Help string
      @param v Default value
      @param arg Whether we need and argument.  */
  Option(char s, const char*  l, const char* h, const T& v=T(), bool arg=true)
    : OptionBase(s, l, h, arg),
      fValue(v)
  {}
  /** Handle an option. If the option matches, decode the value if
      needed and return @c true. 
      @param s Short option character
      @param arg argument to possibly decode to value
      @return @c true if we handled this option */
  bool Handle(char s, const char* arg) 
  {
    if (s != fShort) return false;
    if (!fNeedArg)   {
      std::stringstream s;
      s << true;
      s >> fValue;
      return fSet = true;
    }
    if (!arg)        return true;
    str2val(arg, fValue);
    return fSet = true;
  }
  /** Handle an option. If the option matches, decode the value if
      needed and return @c true. 
      @param l Long option string
      @param arg argument to possibly decode to value
      @return @c true if we handled this option */
  bool Handle(const std::string& l, const std::string& arg) 
  {
    if (l != fLong) return false;
    if (!fNeedArg)   {
      std::stringstream s;
      s << true;
      s >> fValue;
      return fSet = true;
    }
    str2val(arg, fValue);
    return fSet = true;
  }
  /** @return The current value */
  const T& Value() const { return fValue; }
  /** Print help information. */
  void Help() const 
  {
    OptionBase::Help();
    if (NeedArg()) {
      std::cout << "\t[";
      if (!IsSet()) std::cout << "default: ";
      std::cout  << fValue << "]";
    }
    std::cout << std::endl;
  }
  /** Pointer operator to get pointer to underlyinig object 
      @return Pointer to value object  */
  const T* operator->() const { return &fValue; }
  /** Conversion operator.  
      @return This option as value type */
  operator const T&() const { return fValue; }
  /** Assignment operator. 
      @param v Value to assign.
      @return reference to this object */
  Option<T>& operator=(const T& v) { fValue = v; return *this; }
private: 
  /** the current value  */
  T fValue;
};

//____________________________________________________________________
/** @struct CommandLine Options.h <rcudata/Options.h>
    @brief Class to deal with command line options. 
    @ingroup Utils
 */
struct CommandLine
{
  /** Constructor
      @param args Help string  */
  CommandLine(const char* args="") : fArg(args) {}
  /** Process the command line
      @param argc Number of arguments
      @param argv Arguments
      @return @c true on success, false otherwise  */
  bool Process(int argc, char** argv) 
  {
    fProg = argv[0];
    int ret = 0;
    for (int i = 1; i < argc; i++) {
      if (argv[i][0] == '-') {
	if (argv[i][1] == '-') {
	  std::string opt, val;
	  GetLong(argv[i], opt, val);
	  ret = CheckOptions(opt, val);
	}
	else 
	  ret = CheckOptions(argv[i][1], argv[i+1]);
	if (ret < 0) {
	  std::cerr << fProg << ": Unknown option '" << argv[i] 
		    << "', try '" << fProg << " --help'" 
		    << std::endl;
	  return false;
	}
	if (ret > 0) {
	  ret = 0; 
	  i++;
	}
      }
      else fRemain.push_back(argv[i]);
    }
    return true;
  }
  /** @return Vector of remaining arguments not handled by any option */
  std::vector<std::string>& Remain() { return fRemain; }
  /** Add an option 
      @param option Option to add */
  void Add(OptionBase& option) 
  {
    fList.insert(&option);
  }
  /** Print help information. */
  void Help() const
  {
    std::cout << "Usage: " << fProg << " [OPTIONS] " << fArg << "\n\n" 
	      << "Options:"  << std::endl;
    for (OptionList::const_iterator i = fList.begin(); i != fList.end(); i++) 
      (*i)->Help();
    std::cout << std::endl;
  }
protected:
  /** Decode a long option
      @param str option and argument string to decode
      @param opt Decoded option string
      @param val Argument string. */
  void GetLong(const char* str, std::string& opt, std::string& val) 
  {
    std::string arg(str);
    size_t eq = arg.find("=");
    if (eq != std::string::npos) {
      opt = arg.substr(0, eq);
      val = arg.substr(eq+1, arg.size() - eq);
    }
    else 
      opt = arg;
    opt.erase(0,2);
  }
  /** Loop over all defined options, and check if one can handle the
      given short option.  
      @param opt Short option character
      @param arg Possible argument
      @return  negative in case of errors, positive if succcessful,
      and the argument @a arg was processed, or 0 if successful and
      argument @a arg not processed. */
  int CheckOptions(const std::string& opt, const std::string& arg) 
  {
    for (OptionList::iterator i = fList.begin(); i != fList.end(); i++) 
      if ((*i)->Handle(opt, arg)) return 0;
    return -1;
  }
  /** Loop over all defined options, and check if one can handle the
      given long option.  
      @param s short option string
      @param arg Possible argument
      @return  negative in case of errors, positive if succcessful,
      and the argument @a arg was processed, or 0 if successful and
      argument @a arg not processed. */
  int CheckOptions(char s, const char* arg) 
  {
    for (OptionList::iterator i = fList.begin(); i != fList.end(); i++) {
      if ((*i)->Handle(s, arg)) { 
	if ((*i)->NeedArg()) return 1;
	return 0;
      }
    }
    return -1;
  }
  /** Extra help */
  std::string fArg;
  /** Program name */
  std::string fProg;
  /** Type of list of options */
  typedef std::set<OptionBase*> OptionList;
  /** List of options */
  OptionList fList;
  /** List of remaining options. */
  std::vector<std::string> fRemain;
};
  
//____________________________________________________________________
//
// EOF
//
