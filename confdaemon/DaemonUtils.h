// -*- mode: C++ -*-
#ifndef DAEMONUTILS_h
#define DAEMONUTILS_h
#include <dim/dis.hxx>
#include <string>
#include <sstream>
#include <vector>

namespace DaemonUtils
{
  /**
   * Spawn child process.  
   *
   * The child process specied in the @a args parameter of the Execute 
   * member function is executed.  If the child process does not
   * terminate within the timeout specified, then the Execute member
   * function will report it. 
   * 
   */
  class Spawner 
  {
  public:
    /**
     * Error codes
     * 
     */
    enum { 
      /** Fork to child failed */
      kForkFailed, 
      /** Execution of image failed in child */
      kExecFailed, 
      /** Failed to wait for child */
      kWaitFailed, 
      /** The child process timed out */
      kTimeout
    };
    
    /**
     * Type of argument list
     * 
     */
    typedef std::vector<std::string> ArgvList;
    /** 
     * Constructor 
     * 
     * @param timeout Timeout in seconds
     * @param delay   Delay before forking
     */    
    Spawner(unsigned int timeout=60)
      : fTimeout(timeout)
    {}
    /** 
     * Destructor 
     * 
     */    
    virtual ~Spawner() {}
    /** 
     * Execute the image in a child process, and wait til in finishes
     * or it times out. 
     * 
     * @param args Arguments.
     * @param t On return, the time spent 
     * @param delay Time to wait before spawning. 
     * 
     * @return Negative error code or return value from image.
     */    
    virtual int Execute(const ArgvList& args, 
			unsigned int& t,
			unsigned int delay);
    /** 
     * Set the timeout in seconds
     * 
     * @param timeout 
     */
    void SetTimeout(unsigned int timeout) { fTimeout = timeout; }
    /** 
     * Print information to output stream 
     * 
     * @param out Output stream 
     */
    void Print(std::ostream& out) const;
  protected:
    /** Timeout in seconds */ 
    unsigned int fTimeout;
  };

  /**
   * Data structure to hold connection information. 
   * 
   */
  struct Connection 
  {
    /** 
     * Create a connection object.  
     * 
     * @param con Connection string.  It has the format 
     *
     * @verbatim
     *  [NAME@]TARGET[=FILE] 
     * @endverbatim
     * 
     * where 
     * 
     * - NAME is the optional name of the daemon 
     * - TARGET URL to connect to 
     * - FILE is the optional configuration file 
     */
    Connection(const std::string& con)
    {
      size_t at = con.find('@');
      if (at != std::string::npos) { 
	fName   = con.substr(0, at);
	fTarget = con.substr(at+1);
      }
      else { 
	fName = "";
	fTarget = con;
      }
      size_t eq = fTarget.find('=');
      if (eq != std::string::npos) { 
	fFile = fTarget.substr(eq+1);
	fTarget.erase(eq);
      }
    }
    /** 
     * Get the name 
     * 
     * @return The name 
     */
    const std::string& GetName() const { return fName; }
    /** 
     * Get the target 
     * 
     * @return The target 
     */
    const std::string& GetTarget() const { return fTarget; }
    /** 
     * Get the file 
     * 
     * @return The file 
     */
    const std::string& GetFile() const { return fFile; }
    /** Name */
    std::string fName;
    /** Target */
    std::string fTarget;
    /** File */
    std::string fFile;
  };

  /** 
   * @brief Base class for a daemon running in the background and
   * spawns a configuration application on demand.
   *
   * The daemon defines 4 states 
   * 
   * - IDLE         when doing nothing, waiting for commands 
   * - CONFIGURING  when executing child process 
   * - DONE         after successful execution of child process
   * - RECOVERY     `stand-by' state where things may happen underneath. 
   * - ERROR        after failed execution of chipd process. 
   * 
   * The daemon defines a number of actions in each state 
   *
   * - In IDLE  The commands accepted depends on the concrete
   *   implementation of the daemon and child process.  The daemon
   *   will switch to state CONFIGURING if the action is accepted.
   *   Otherwise the daemon will go to ERROR 
   * - In CONFIGURING no actions are allowed. 
   * - In DONE the action ACKNOWLEDGE is accepted and will change the
   *   state to IDLE.  This implements a handshake. 
   * - In ERROR, only the action CLEAR is allowed.  This will reset
   *   the state to IDLE.
   * - In the IDLE state, the action RECOVER is accepted and will
   *   switch the daemons state to RECOVERY. This state is meant to
   *   signal clients that something is going on.
   * - In the state RECOVERY the only allowed action is CLEAR.  This
   *   will move the daemon to the state IDLE.
   *
   * The SMI state machine for this daemon will look like 
   *
   * @verbatim 
   *   object: DAEMON / associated
   *   state: OFF / initial_state
   *   state: IDLE 
   *       action: RECOVER
   *       action: implementation_specific 
   *   state: CONFIGURING
   *       action: FAIL
   *       action: SUCCESS
   *   state: RECOVERY
   *       action: FAIL
   *       action: SUCCESS
   *   state: DONE
   *       action: ACKNOWLEDGE
   *   state: ERROR
   *       action: CLEAR    
   * @endverbatim 
   */
  class Base : public DimCommandHandler, DimErrorHandler
  {
  public:
    /** Destructor */
    virtual ~Base();
    /** Run this daemon 
	@return 0 on success, error code otherwise */
    int Run();
    /** 
     * Whether to use the DONE state 
     * 
     * @param use If true, use the DONE state 
     */
    void UseDone(bool use=true) { fUseDone = use; }
    /** 
     * Whether to enable the RECOVERY state 
     * 
     * @param use If true, enable the RECOVERY state
     */
    void UseRecovery(bool use=true) { fUseRecovery = use; }
    /** 
     * The package name 
     * 
     * @return Package name 
     */
    static const char* PackageName();
    /** 
     * The package version
     * 
     * @return Package version
     */
    static const char* PackageVersion();
    /** 
     * The package name and version
     * 
     * @return Package name and version
     */
    static const char* PackageString();
    /** 
     * DIM version 
     * 
     * @return DIM version 
     */
    static unsigned short DimVersion();
    /** 
     * Print information to a stream 
     * 
     * @param o Output stream to print to 
     */
    virtual void Print(std::ostream& o) const;
  protected:
    /** Message structure */
    struct MsgContent_t { 
      unsigned short fLevel;
      char           fMsg[1024];
    } fMsgContent;
    enum State_t { 
      /** The 'idle' - or 'waiting' - state. */
      kIdle = 1, 
      /** The 'configuring' - or 'spawning' state */ 
      kConfiguring, 
      /** The 'done' state - command succeded */
      kDone,
      /** The 'recovery' state - recovering problems */
      kRecovery,
      /** The 'error' state */ 
      kError
    };
    /** Command types */
    enum Cmd_t { 
      /** Clear command */ 
      kClear = 1, 
      /** Acknowledge command */ 
      kAcknowledge, 
      /** Recover command */ 
      kRecover,
      /** User defined command */ 
      kOther,
      /** Notification of failure */
      kFail,
      /** Notification of success */
      kSuccess
    };
    
    /** String of Idle state */
    static const char* fgIdle;
    /** String of Configuring state */
    static const char* fgConfiguring;
    /** String of Done state */
    static const char* fgDone;
    /** String of Done state */
    static const char* fgRecovery;
    /** String of Error state */
    static const char* fgError;
    /** Message types */
    enum Msg_t { 
      /** Information */
      kMsgInfo, 
      /** Warning */
      kMsgWarning, 
      /** Error */
      kMsgError 
    };
    /** 
     *  Constructor 
     * 
     * @param prefix   Name prefix 
     * @param dns      DIM domain name server host
     * @param url      Connection URL for our FEE
     * @param target   Target FEE 
     * @param bindir   Directory where the slave program is installed
     * @param pidfile  File to store the process identifier in. 
     * @param timeout  Timeout in seconds of slave program
     * @param delay    Delay before starting slave program
     * @param retry    Wether to retry in case of problems 
     * 
     * @return 
     */
    Base(const std::string& prefix,
	 const std::string& dns, 
	 const std::string& url,
	 const std::string& target,
	 const std::string& bindir,
	 const std::string& pidfile,
	 unsigned int       timeout,
	 unsigned int       delay,
	 bool               retry=true);
    /** 
     * This will set-up the basic services of daemon
     * 
     */
    void SetupBaseServices();
    /** 
     * Derived classes should implement this method to set up any
     * additional services needed.
     * 
     */
    virtual void SetupAuxServices() = 0;
    /** Make a message 
	@param t  Message type 
	@param format printf-like format string */
    void Message(Msg_t t, const char* format, ...);

    virtual void SetChildReturn(int) { };
    /** 
     * Set the current state 
     * @param state  State number 
     */ 
    void SetState(State_t state);
    /** 
     * Make a service name 
     */
    void MakeServiceName(const char* what, std::string& out);
    /** 
     * Make a pid file entry 
     */ 
    void MakePIDFile(const std::string& pidfile);
    /** 
     * Extract a part form the command string 
     * 
     * @param what  What we're looking for 
     * @param s     The stream to read from 
     * @param out   The output value 
     */    
    void GetPart(const char*        what, 
		 std::stringstream& s, 
		 std::string&       out);
    /** 
     * Extract a part form the command string 
     * 
     * @param what  What we're looking for 
     * @param s     The stream to read from 
     * @param out   The output value 
     */    
    void GetPart(const char*        what, 
		 std::stringstream& s, 
		 unsigned int&      out);
    /** 
     * Get a field value 
     * 
     * @param what What to get 
     * @param str  string to read from 
     * @param val  On return, the value 
     * 
     * @return true if field is found and successfully decoded 
     */
    bool GetField(const char*        what, 
		  const std::string& str, 
		  std::string&       val) const;
    /** 
     * Get a field value 
     * 
     * @param what What to get 
     * @param str  string to read from 
     * @param val  On return, the value 
     * 
     * @return true if field is found and successfully decoded 
     */
    template <typename T> 
    bool GetValue(const char* what, 
		  const std::string& str, 
		  T&                 val) const;
    /** 
     * Get the command type 
     * 
     * @param first Full command string (downcased)
     * 
     * @return The command type 
     */
    virtual unsigned short GetCommand(const std::string& first) const;
    /** 
     * Make the command line 
     * 
     * @param first The full command string (all lower case)
     * @param args  Command line arguments
     */
    virtual void MakeCommandLine(const std::string& first, 
				 std::vector<std::string>& args) = 0;
    /** 
     * Handle incomming command 
     */
    void commandHandler();
    /** 
     * Handle DIM errors
     * 
     * @param severity   Error severity
     * @param errorCode  Error code 
     * @param errorMsg   Message
     */
    void errorHandler(int severity, int errorCode, char *errorMsg);
    /** Spawner */
    Spawner fSpawner;
    /** Name of server */ 
    std::string fName;
    /** Our DIM DNS node */
    const std::string fDns;
    /** The URL we use to connect to the FEE */
    const std::string fUrl;
    /** Our target fee */
    const std::string fTarget;
    /** Directory that holds executable */
    const std::string fBinDir;
    /** how many times to try the configuration command */
    unsigned int fNTries;
    /** The state stored */
    unsigned int fCurrentState;
    /** Our Command */
    DimCommand* fGo;
    /** Our state service */
    DimService* fState;
    /** Our message service */
    DimService* fMsg;
    /** Dim error level */
    int fDIMErrorLvl;
    /** Dim error code */
    int fDIMErrorCde;
    /** DIM error message */ 
    std::string fDIMErrorMsg;
    /** Whether to use the 'done' state or not */
    bool fUseDone;
    /** Use recovery state to signal recovery is taking place */ 
    bool fUseRecovery;
    /** Delay in seconds */
    unsigned int fSpawnDelay;
  };

  template <typename T> 
  inline bool
  Base::GetValue(const char*        what, 
		 const std::string& str, 
		 T&                 val) const
  {
    std::string value;
    if (!GetField(what, str, value)) return false;
    
    std::stringstream s(value);
    s >> val;
    return true;
  }
}


#endif
//
// EOF
//


