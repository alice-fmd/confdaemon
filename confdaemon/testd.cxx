#include "config.h"
#include "Options.h"
#include "DaemonUtils.h"
#ifndef PACKAGE_URL
# define PACKAGE_URL "http://fmd.nbi.dk/fmd/fee/software.html#miniconf"
#endif

class Daemon : public DaemonUtils::Base
{
public:
  Daemon(const std::string& dns, 
	 const std::string& tgt, 
	 const std::string& bindir, 
	 const std::string& pidfile,
	 unsigned int       timeout, 
	 unsigned int       delay,
	 bool               retry) 
    : Base("TEST", dns, "echo:?fwvers=0x10000", 
	   tgt, bindir, pidfile, timeout, delay, retry) 
  {}
protected:
  void SetupAuxServices() {}
  void MakeCommandLine(const std::string& full,
		       std::vector<std::string>&) 
  { 
    std::cout << "Command string: " << full << std::endl;
  }
};

int
main(int argc, char** argv)
{
  
  Option<bool>        hOpt('h', "help",     "Show this help", false,false);
  Option<bool>        VOpt('V', "version",  "Show version number",false,false);
  Option<bool>        rOpt('r', "retry",    "Retry command", false,false);
  Option<bool>        aOpt('a', "enable-done","Enable done state", false,false);
  Option<bool>        ROpt('R', "enable-recovery","Enable recovery state", 
			   false,false);
  Option<std::string> nOpt('n', "base",     "Base name", "MINICONF");
  Option<std::string> DOpt('D', "dns",      "DIM DNS node", "alifmddimdns");
  Option<std::string> NOpt('N', "name",     "Name of server", "FMD-FEE_0_0_0");
  Option<std::string> tOpt('t', "url",      "URL to connect to", "");
  Option<std::string> pOpt('p', "pid",      "Pid file");
  Option<std::string> BOpt('B', "bindir",   "Directory holding miniconf", 
			   "/usr/bin");
  Option<unsigned>    TOpt('T', "timeout",  "Time out in seconds of miniconf",
			   120);
  Option<unsigned>    WOpt('W', "delay",    "Delay before executing miniconf",
			   10);
  
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(ROpt);
  cl.Add(rOpt);
  cl.Add(NOpt);
  cl.Add(tOpt);
  cl.Add(nOpt);
  cl.Add(DOpt);
  cl.Add(pOpt);
  cl.Add(BOpt);
  cl.Add(TOpt);
  cl.Add(WOpt);
  cl.Add(aOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << PACKAGE_STRING << std::endl;
    return 0;
  }

  std::string pfx = nOpt;
  std::string dns = DOpt;
  std::string nme = NOpt;
  std::string pid = pOpt;
  std::string bin = BOpt;
  std::string tgt = tOpt;
  bool        rty = rOpt;
  bool        rec = ROpt;
  unsigned    tmo = TOpt;
  unsigned    dly = WOpt;
  bool        dne = aOpt;
  std::cout << PACKAGE_STRING    << "\n"
	    << PACKAGE_URL       << "\n"
	    << PACKAGE_BUGREPORT << "\n"
	    << "Executing daemon:\n"
	    << "\tPrefix:        " << pfx << "\n"
	    << "\tDNS:           " << dns << "\n"
	    << "\tName:          " << nme << "\n"
	    << "\tTarget:        " << tgt << "\n"
	    << "\tPID file:      " << pid << "\n"
	    << "\tRetry:         " << rty << "\n"
	    << "\tRecovery state:" << rec << "\n"
	    << "\tTimeout:       " << tmo << "s\n" 
	    << "\tDelay:         " << dly << "s\n"
	    << "\tDone state:    " << dne << "\n"
	    << std::endl;

  Daemon daemon(dns, tgt, bin, pid, tmo, dly, rty);
  daemon.UseDone(dne);
  daemon.UseRecovery(rec);
  
  return daemon.Run();
}

//
// EOF
//

  
