#include <confdaemon/DaemonUtils.h>
#include <iostream>

void
test(const std::string& line)
{
  DaemonUtils::Connection c(line);
  std::cout << "Input is:  " << line << "\n"
	    << "   Name:   " << c.GetName() << "\n"
	    << "   Target: " << c.GetTarget() << "\n"
	    << "   File:   " << c.GetFile() << std::endl;
}

int 
main(int argc, char** argv)
{
  if (argc > 1) { 
    for (int i = 1; i < argc; i++) test(argv[i]);
    return 0;
  }

  const char* tests[] = { 
    "FMD-FEE_0_0_1@fee://alifmddimdns/FMD-FEE_0_0_0", 
    "FMD-FEE_0_0_2@rorc://4096:4:0=/foo/bar/baz.dl", 
    "FMD-FEE_0_0_3", 
    "@fee://alifmddimdns/FMD-FEE_0_0_0", 
    0
  };
  
  const char** ptr = tests;
  while (*ptr) { 
    test(*ptr);
    ptr++;
  }

  return 0;
}

  
