#include "DaemonUtils.h"
#ifdef HAVE_CONFIG_H
# include "config.h"
#else
# ifndef PACKAGE_NAME
#  define PACKAGE_NAME    "ConfDaemon"
# endif
# ifndef PACKAGE_VERSION
#  define PACKAGE_VERISON "?.?"
# endif
# ifndef PACKAGE_STRING
#  define PACKAGE_STRING  "ConfDaemon ?.?"
# endif
#endif

#include <dim/dim.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

#include <algorithm>
#include <iterator>
#include <stdexcept>

#include <cstdlib>
#include <cmath>
#include <cctype>
#include <cstdarg>

#include <unistd.h>
#include <sys/wait.h>

//====================================================================
namespace 
{
  //____________________________________________________________________
#ifdef __UNIX__
  struct DimGuard
  {
    DimGuard() 
    {
      sigemptyset(&fThisSet);
      sigaddset(&fThisSet, SIGIO);
      sigaddset(&fThisSet, SIGALRM);
      sigprocmask(SIG_BLOCK, &fThisSet, &fOldSet);
      dim_lock();
    }
    ~DimGuard() 
    {
      dim_unlock();
      sigprocsetmask(SIG_SETMASK, &fOldSet, 0);
    }
    sigset fThisSet;
    sigset fOldSet;
  };
#else 
  struct DimGuard 
  {
    DimGuard() { dim_lock(); }
    ~DimGuard() { dim_unlock(); }
  };
#endif
  
  //____________________________________________________________________
  struct to_lower 
  {
    char operator()(char c) const { return std::tolower(c); }
  };

  //____________________________________________________________________
  void
  getFullCommand(const std::vector<std::string>& args, std::string& cmd)
  {
    std::stringstream s;
    for (size_t i = 0; i < args.size(); i++) { 
      s << args[i];
      if (i != (args.size() - 1)) s << " ";
    }
    cmd = s.str();
  }

  struct dim_exception : public std::exception
  {
    dim_exception(int severity, const char* what) 
      : _what(what), _lvl(severity)
    {
    }
    virtual ~dim_exception() throw () {}
    const char* what() const throw() { return _what.c_str(); } 
    bool is_info()     const throw() { return _lvl == DIM_INFO; }
    bool is_warning()  const throw() { return _lvl == DIM_WARNING; }
    bool is_error()    const throw() { return _lvl == DIM_ERROR; }
    bool is_fatal()    const throw() { return _lvl == DIM_FATAL; } 
  protected:
    std::string _what;
    int         _lvl;
  };
}

//====================================================================
int
DaemonUtils::Spawner::Execute(const ArgvList& args, 
			      unsigned int& t,
			      unsigned int delay)
{
  // Delay execution 
  if (delay > 1) {
    std::cout << "Waiting " << delay << " seconds before spawing" << std::endl;
    sleep(delay);
  }

  // Reset time 
  t = 0;
  
  // Fork the executable 
  pid_t chpid = fork();
  
  // Check for errors
  if (chpid < 0) return -kForkFailed;

  // Check where we are 
  if (chpid == 0) { 
    // Child process 
    static char* argv[32];
    for (size_t i = 0; i < args.size(); i++) 
      argv[i] = const_cast<char*>(args[i].c_str());
    argv[args.size()] = 0;

    int ret = execv(args[0].c_str(), argv);
    if (ret < 0) return -kExecFailed;
    return ret;
  }
  
  // Parent process 
  int   chret  = -1;
  int   status = 0;
  pid_t wpid;

  // Wait for child
  for (; t < fTimeout; t++) { 
    // Do a non-blocking wait.  Also get status of child it is done 
    wpid = waitpid(chpid, &status, WNOHANG | WUNTRACED);
    
    // Error 
    if (wpid < 0) return -kWaitFailed;
    
    if (wpid == chpid) { 
      // Got the exit 
      chret = (WIFEXITED(status)) ? WEXITSTATUS(status) : -1;
      break;
    }
    
    // Sleep for 1 second 
    sleep(1);
  }
  
  if (wpid == 0) { 
    // Timeout 
    kill(chpid, SIGKILL);
    return -kTimeout;
  }
  
  // Normal exit - return child return value
  return chret;
}
//____________________________________________________________________
void
DaemonUtils::Spawner::Print(std::ostream& o) const
{
  o << "\tTimeout (seconds):  " << fTimeout << std::endl;
}

//====================================================================
const char* DaemonUtils::Base::fgIdle        = "idle";
const char* DaemonUtils::Base::fgConfiguring = "configuring";
const char* DaemonUtils::Base::fgDone        = "done";
const char* DaemonUtils::Base::fgError       = "error";
const char* DaemonUtils::Base::fgRecovery    = "recovery";

//____________________________________________________________________
DaemonUtils::Base::Base(const std::string& prefix, 
			const std::string& dns, 
			const std::string& url,
			const std::string& target, 
			const std::string& bindir,
			const std::string& pidfile,
			unsigned int       timeout,
			unsigned int       delay,
			bool               retry)
  : fSpawner(timeout), 
    fName(""),
    fDns(dns), 
    fUrl(url),
    fTarget(target), 
    fBinDir(bindir),
    fNTries(retry ? 3 : 1),
    fCurrentState(kIdle),
    fGo(0),
    fState(0), 
    fMsg(0), 
    fDIMErrorLvl(0),
    fDIMErrorCde(0),
    fDIMErrorMsg(""),
    fUseDone(false),
    fUseRecovery(false),
    fSpawnDelay(delay)
{
  // Set the DIM DNS node
  DimServer::setDnsNode(const_cast<char*>(fDns.c_str()));
  DimServer::addErrorHandler(this);
  
  // Make server name 
  std::stringstream s;
  s << prefix << "_" << fTarget;
  fName = s.str();
  
  // Make the PID file
  MakePIDFile(pidfile);
}

//____________________________________________________________________
DaemonUtils::Base::~Base()
{
  if (fGo)    delete fGo;
  if (fState) delete fState;
  if (fMsg)   delete fMsg;
}

//____________________________________________________________________
void
DaemonUtils::Base::SetupBaseServices()
{
  if (fGo)    delete fGo;
  if (fState) delete fState;
  if (fMsg)   delete fMsg;

  // Construct service names
  std::string cmd_name;
  std::string state_name;
  std::string msg_name;
  MakeServiceName("GO",      cmd_name);
  MakeServiceName("STATE",   state_name);
  MakeServiceName("MSG",     msg_name);

  // Make service and command
  const char* cformat = "C";
  const char* mformat = "S:C";
  fMsgContent.fLevel = kMsgInfo;
  int len = snprintf(fMsgContent.fMsg, 1024, "%s starting up", fName.c_str());

  fGo    = new DimCommand(cmd_name.c_str(), const_cast<char*>(cformat), this);
  fState = new DimService(state_name.c_str(), const_cast<char*>(fgIdle));
  fMsg   = new DimService(msg_name.c_str(), const_cast<char*>(mformat),
			  &fMsgContent, sizeof(short)+len+1);
  
}

//____________________________________________________________________
int
DaemonUtils::Base::Run()
{
  while (true) { 
    try { 
      SetupBaseServices();
      SetupAuxServices();
      DimServer::start(const_cast<char*>(fName.c_str()));
      while (true) { 
	sleep(5);
	if (fDIMErrorLvl != 0) { 
	  // std::cerr << "DIM ";
	  bool  ret  = false;
	  bool  err  = false;
	  Msg_t type = kMsgInfo;
	  std::string stype = "Info";
	  switch (fDIMErrorLvl) { 
	  case DIM_INFO:
	    stype = "Info";
	    type  = kMsgInfo;
	    break;
	  case DIM_WARNING: 
	    stype = "Warning";
	    type  = kMsgWarning;
	    break;
	  case DIM_ERROR:   
	    stype = "Error";
	    type  = kMsgError;
	    err   = true;
	    break;
	  case DIM_FATAL: 
	    stype = "Fatal";
	    type  = kMsgError;
	    err   = true;
	    ret   = true;
	    break;
	  }
	  Message(type, "DIM %s: %s", stype.c_str(), fDIMErrorMsg.c_str());
	  // if (err) SetState(kError);
	  if (ret) return 1;
	}
      }
    }
    catch (std::exception& e) { 
      std::cerr << e.what() << std::endl;
      SetState(kError);
      sleep(5);
      continue;
    }
  }
  return 0;
}

//____________________________________________________________________
void
DaemonUtils::Base::Message(Msg_t type, const char* format, ...)
{
  std::ostream& out = (type == kMsgError ? std::cerr : std::cout);
  va_list ap;
  va_start(ap, format);
  const int   nbuf = 1024;
  char        tbuf[256];
  int         len       = vsnprintf(fMsgContent.fMsg, nbuf, format, ap);
  time_t      now       = time(NULL);
  struct tm*  loctime   = localtime(&now);
  len                   = (len >= nbuf ? nbuf-1 : len);
  fMsgContent.fMsg[len] = '\0';
  strftime(tbuf, 256, "%d %b %Y %T", loctime);
  out << fName << " [" << tbuf << "] - " 
      << (type == kMsgError ? "Error" : 
	  (type == kMsgWarning ? "Warning" : "Info")) << ": " 
      << fMsgContent.fMsg << std::flush;
  if (fMsgContent.fMsg[len-1] == '\n') fMsgContent.fMsg[--len] = '\0';
  fMsgContent.fLevel = type;
  if (fMsg) 
    fMsg->updateService(&fMsgContent, sizeof(short)+len+1);
  va_end(ap);
}

//____________________________________________________________________
void
DaemonUtils::Base::MakeServiceName(const char* what, std::string& out)
{
  std::stringstream s;
  s << fName << "/" << what;
  out = s.str();
}

//____________________________________________________________________
void
DaemonUtils::Base::MakePIDFile(const std::string& pidfile)
{
  if (pidfile.empty()) return;
  
  std::ofstream pidout(pidfile.c_str(), std::ios_base::app);
  if (!pidout) 
    throw std::runtime_error("Failed to open PID file");
    
  pid_t pid = getpid();
  pidout << pid << " " << std::flush;
  pidout.close();
}

//____________________________________________________________________
void
DaemonUtils::Base::SetState(State_t state)
{
  const char*  newval = 0;
  unsigned int next   = state;
  if (!fUseDone     && state == kDone)     next = kIdle;
  if (!fUseRecovery && state == kRecovery) next = kIdle;
  switch (next) { 
  case kIdle:        newval = fgIdle;        break;
  case kConfiguring: newval = fgConfiguring; break;
  case kDone:        newval = fgDone;        break;
  case kRecovery:    newval = fgRecovery;    break;
  default:           newval = fgError;       next = kError; break;
  }
  fCurrentState = next;
  fState->setTimestamp(time(NULL), 0);
  fState->updateService(const_cast<char*>(newval));
}

//____________________________________________________________________
void
DaemonUtils::Base::GetPart(const char*        what, 
			   std::stringstream& s, 
			   std::string&       out)
{
  std::getline(s, out, ';');
  if (s.eof() || !s.bad()) return;

  std::stringstream e;
  Message(kMsgError, "Failed to read %s from command buffer\n", what);
  throw std::runtime_error(e.str().c_str());
}

//____________________________________________________________________
void
DaemonUtils::Base::GetPart(const char*        what, 
			   std::stringstream& s, 
			   unsigned int&      out)
{
  s << std::dec;
  if (s.peek() == '0') { 
    s.get();
    s << std::oct;
    if (s.peek() == 'x' || s.peek() == 'X') { 
      s.get();
      s << std::hex;
    }
  }
  s >> out;
  if (!s.bad() && (s.eof() || s.get() == ';')) return;
  
  std::stringstream e;
  e << "Failed to read " << what << " from command buffer" << std::endl;
  throw std::runtime_error(e.str().c_str());
}

//____________________________________________________________________
bool
DaemonUtils::Base::GetField(const char*        what, 
			    const std::string& str, 
			    std::string&       val) const
{
  if  (str.find(what) == std::string::npos) return false;
  
  size_t eq = str.find("=");
  if (eq == std::string::npos) return true;
  
  std::string s(str.substr(eq+1, str.size()-eq-1));
  val = s;
  return true;
}

//____________________________________________________________________
unsigned short 
DaemonUtils::Base::GetCommand(const std::string& first) const
{
  if      (first == "clear")       return kClear;
  else if (first == "acknowledge") return kAcknowledge;
  else if (first == "recover")     return kRecover;
  else if (first == "fail")        return kFail;
  else if (first == "success")     return kSuccess;
  return kOther;
}

//____________________________________________________________________
void 
DaemonUtils::Base::commandHandler()
{
  // DISABLE_AST
#if 0
  ; // For Emacs
#endif
  DimGuard guard;
  try { 
    if (fCurrentState == kConfiguring) {
      Message(kMsgWarning, "Cannot accept actions while configuring");
      return;
    }

    char* str = fGo->getString();
    if (!str || str[0] == '\0') { 
      Message(kMsgError, "Command string is empty!");
      return;
    }
    Message(kMsgInfo, "Command string is \"%s\"\n", str);

    std::string first(str);
    std::transform(first.begin(),first.end(),first.begin(),to_lower());
    if (first[first.size()-1] == ';') 
      first.erase(first.size()-1);
    unsigned int icmd = GetCommand(first);
    if (icmd == kClear) { 
      if (fCurrentState == kError) {
	Message(kMsgInfo, "Cleared state");
	SetState(kIdle);
      }
      else {
	Message(kMsgWarning, 
		"Action 'clear' does not make sense in this state");
      }
      return;
    }
    if (icmd == kRecover) { 
      if (fCurrentState != kIdle) { 
	Message(kMsgWarning, 
		"Action 'recovery' does not make sense in this state");
      }
      else if (!fUseRecovery) { 
	Message(kMsgInfo, "Recovery state disabled for this daemon");
      }
      else {
	Message(kMsgInfo, "Enabling recovery state");
	SetState(kRecovery);
      }
      return;
    }
    if (icmd == kAcknowledge) { 
      if (fCurrentState != kDone) { 
	Message(kMsgError, "Cannot acknowledge in current state");
	SetState(kError);
      }
      else {
	Message(kMsgInfo, "Got acknowledgement of done state, going idle");
	SetState(kIdle);
      }
      return;
    }
    if (icmd == kSuccess) {
      if (fCurrentState != kRecovery || fCurrentState != kConfiguring) {
	Message(kMsgWarning, 
		"Action 'success' does not make sense in this state");
      }
      else {
	Message(kMsgInfo, "Got success, going to done");
	SetState(kDone);
      }
      return;
    }
    if (icmd == kFail) {
      if (fCurrentState != kRecovery || fCurrentState != kConfiguring) {
	Message(kMsgWarning, 
		"Action 'fail' does not make sense in this state");
      }
      else {
	Message(kMsgInfo, "Got failure, going to error");
	SetState(kError);
      }
      return;
    }
	
    if (fCurrentState != kIdle) {
      Message(kMsgWarning, "Cannot accept custom action while not idle");
      return;
    }
    std::vector<std::string> args;
    MakeCommandLine(first, args);
    
    if (args.size() <= 0) { 
      Message(kMsgWarning, "Command is empty!\n");
      return;
    }
    std::string cmd;
    getFullCommand(args, cmd);
    
    
    SetState(kConfiguring);
    
    volatile int ret = 0;
    unsigned int d   = fSpawnDelay;
    if (first.find("reinitialize") != std::string::npos) { 
      // Cut down delay in this case 
      d = 1;
    }
    Message(kMsgInfo, "Will try %d times\n", fNTries);
    for (unsigned int i = 0; i < fNTries; i++) { 
      Message(kMsgInfo, "Executing \"%s\" (%d%s of %d tries)\n",
	      cmd.c_str(), i+1, 
	      (i == 0 ? "st" : (i == 1 ? "nd" : (i == 2 ? "rd" : "th"))),
	      fNTries);

      unsigned int t = 0;
      ret            = fSpawner.Execute(args, t, d);
      
      Message((ret == 0 ? kMsgInfo : kMsgWarning), 
	      "Command \"%s\" returned %d after %d secs\n", 
	      cmd.c_str(),ret,t);
      if (ret == 0) break;
      
      sleep(1);
    }
    SetChildReturn(ret);
    SetState(ret == 0 ? kDone : kError);
    Message(ret == 0 ? kMsgInfo : kMsgError, 
	    "Command \"%s\" %s [%d]\n", cmd.c_str(), 
	    (ret <= 0 ? "succeeded" : "failed"), ret);
  }
  catch (std::exception& e) {
    Message(kMsgError, "%s", e.what());
    SetState(kError);
    sleep(5);
  }
  // ENABLE_AST
#if 0
    ; // For Emacs
#endif
}

//____________________________________________________________________
void 
DaemonUtils::Base::errorHandler(int severity, int errorCode, char *errorMsg)
{
  // Message(kMsgInfo, "Error handler invoked with %d, %d, %s", 
  // severity, errorCode, errorMsg);
  fDIMErrorLvl = severity;
  fDIMErrorCde = errorCode;
  fDIMErrorMsg = errorMsg;
  // throw dim_exception(severity, errorMsg);
}

//____________________________________________________________________
const char*
DaemonUtils::Base::PackageName() 
{
  return PACKAGE_NAME;
}
//____________________________________________________________________
const char*
DaemonUtils::Base::PackageVersion()
{
  return PACKAGE_VERSION;
}
//____________________________________________________________________
const char*
DaemonUtils::Base::PackageString()
{
  return PACKAGE_STRING;
}
//____________________________________________________________________
unsigned short
DaemonUtils::Base::DimVersion()
{
  return DIM_VERSION_NUMBER;
}

//____________________________________________________________________
void
DaemonUtils::Base::Print(std::ostream& o) const
{
  o << PackageString() << "\n"
    << std::boolalpha 
    << "\tDIM:                " << DimVersion() << '\n' 
    << "\tDNS:                " << fDns << '\n'
    << "\tURL:                " << fUrl << '\n'
    << "\tServer name:        " << fName << '\n'
    << "\tTarget FEE:         " << fTarget << '\n'
    << "\tBin directory:      " << fBinDir << '\n'
    << "\t# of tries:         " << fNTries << '\n'
    << "\tUse done state:     " << fUseDone << '\n'
    << "\tUse recovery state: " << fUseRecovery 
    << std::noboolalpha << std::endl;
  fSpawner.Print(o);
}

//____________________________________________________________________
//
// EOF
//
