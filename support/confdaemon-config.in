#!/bin/sh
#
#  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
#
#  This library is free software; you can redistribute it and/or 
#  modify it under the terms of the GNU Lesser General Public License 
#  as published by the Free Software Foundation; either version 2.1 
#  of the License, or (at your option) any later version. 
#
#  This library is distributed in the hope that it will be useful, 
#  but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
#  Lesser General Public License for more details. 
# 
#  You should have received a copy of the GNU Lesser General Public 
#  License along with this library; if not, write to the Free 
#  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
#  02111-1307 USA 
#

prefix=@prefix@
exec_prefix=@exec_prefix@
bindir=@bindir@
includedir=@includedir@ 
pkgincludedir=@pkgincludedir@ 
libdir=@libdir@
pkglibdir=@pkglibdir@
datarootdir=@datarootdir@
datadir=@datadir@
pkgdatadir=@datadir@/@PACKAGE@
package="@PACKAGE_TARNAME@"
string="@PACKAGE_STRING@"
email="@PACKAGE_BUGREPORT@"
version="@VERSION@"
extraldflags="@extraldflags@"

prog=`basename $0`

if test $# -eq 0 ; then 
    $0 --help
    exit 0
fi

while test $# -gt 0 ; do 
    case $1 in 
	--*=*)    
	    opt=`echo $1 | sed 's/=.*//'`
	    arg=`echo $1 | sed 's/[^=]*=//'` 
	    ;;
	--*)
	    opt=$1
	    arg=
	    ;;
	-*)
	    opt=$1
	    arg=$2
	    ;;
	*)
	    echo "$prog: unknown argument '$1', try '$prog --help'"
	    exit 1
	    ;;
    esac

    case $opt in
	--help|-h) 
	    cat <<EOF
Usage: $prog [options]

  --prefix          Gives the installation prefix
  --libdir          Gives path to libraries
  --includedir      Gives path to headers 
  --libs            Gives library information 
  --ltlibs	    Gives libtool library information
  --ldflags         Gives linkage information 
  --ltldflags       Gives libtool linkage information 
  --cppflags        Gives preprocessor information
  --datadir	    Gives the data directory 
  --version         Gives version information
  --help            Gives this help
EOF
	    ;;
	--version|-v)
	    cat <<EOF
$string 

Copyright 2006 Christian Holm Christensen <$email>
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
EOF
	    exit 0
	    ;;
	-V)
	    echo $version 
	    exit 0
	    ;;
	--prefix|-p)
	    out="$out $prefix" 
	    ;;
	--libdir|-L)
	    out="$out $libdir"
	    ;;
	--includedir|-I)
	    out="$out $includedir" 
	    ;;
	--cppflags|-D)
	    if test ! "x$includedir" = "x/usr/include" ; then 
		out="$out -I$includedir" 
	    fi
	    ;;
	--ldflags) 
	    if test ! "x$libdir" = "x/usr/lib" ; then 
		out="$out @DIM_LDFLAGS@ -L$libdir" 
	    fi
	    out="$out $extraldflags -lconfdaemon" 
	    ;;
	--libs|-l)
	    out="$out -lconfdaemon -ldim" 
	    ;;
	--ltlibs)
	    out="$out $libdir/libconfdaemon.la"
	    ;;
	--ltldflags)
	    if test ! "x$libdir" = "x/usr/lib" ; then 
		out="$out -R $libdir" 
	    fi
	    out="$out $extraldflags"
	    ;;
	--datadir)
	    out="$out $pkgdatadir"
	    ;;
	--bindir)
	    out="$out $bindir"
	    ;; 
	*)
	    echo "$prog: unknown option '$opt' - try '$prog --help'"
	    exit 1
    esac
    shift
done
		
echo $out	
		
	
#
# EOF
#
