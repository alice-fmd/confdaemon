dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: confdaemon.m4,v 1.3 2012-03-06 09:39:14 cholm Exp $ 
dnl  
dnl  ROOT generic confdaemon framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl
dnl __________________________________________________________________
dnl
dnl AC_CONFDAEMON([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_CONFDAEMON],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([confdaemon],
        [AC_HELP_STRING([--with-confdaemon],	
	                [Prefix where ConfDaemon is installed])],
	[],[with_confdaemon="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([confdaemon-url],
        [AC_HELP_STRING([--with-confdaemon-url],
		[Base URL where the ConfDaemon dodumentation is installed])],
        confdaemon_url=$withval, confdaemon_url="")
    if test "x${CONFDAEMON_CONFIG+set}" != xset ; then 
        if test "x$with_confdaemon" != "xno" ; then 
	    CONFDAEMON_CONFIG=$with_confdaemon/bin/confdaemon-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_confdaemon" != "xno" ; then 
        AC_PATH_PROG(CONFDAEMON_CONFIG, confdaemon-config, no)
        confdaemon_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for ConfDaemon version >= $confdaemon_min_version)

        # Check if we got the script
        with_confdaemon=no    
        if test "x$CONFDAEMON_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           CONFDAEMON_CPPFLAGS=`$CONFDAEMON_CONFIG --cppflags`
           CONFDAEMON_INCLUDEDIR=`$CONFDAEMON_CONFIG --includedir`
           CONFDAEMON_LIBS=`$CONFDAEMON_CONFIG --libs`
           CONFDAEMON_LTLIBS=`$CONFDAEMON_CONFIG --ltlibs`
           CONFDAEMON_LIBDIR=`$CONFDAEMON_CONFIG --libdir`
           CONFDAEMON_LDFLAGS=`$CONFDAEMON_CONFIG --ldflags`
           CONFDAEMON_LTLDFLAGS=`$CONFDAEMON_CONFIG --ltldflags`
           CONFDAEMON_PREFIX=`$CONFDAEMON_CONFIG --prefix` 
           CONFDAEMON_BINDIR=`$CONFDAEMON_CONFIG --bindir` 
           CONFDAEMON_DATADIR=`$CONFDAEMON_CONFIG --datadir` 
          
           # Check the version number is OK.
           confdaemon_version=`$CONFDAEMON_CONFIG -V` 
           confdaemon_vers=`echo $confdaemon_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           confdaemon_regu=`echo $confdaemon_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $confdaemon_vers -ge $confdaemon_regu ; then 
                with_confdaemon=yes
           fi
        fi
        AC_MSG_RESULT($with_confdaemon - is $confdaemon_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_CONFDAEMON, [Whether we have confdaemon])
    
    
        if test "x$with_confdaemon" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
            LDFLAGS="$LDFLAGS $CONFDAEMON_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $CONFDAEMON_CPPFLAGS"
     
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_confdaemon_daemonutils_h=0
            AC_CHECK_HEADER([confdaemon/DaemonUtils.h], 
                            [have_confdaemon_daemonutils_h=1])
    
            # Check the library. 
            have_libconfdaemon=no
            AC_MSG_CHECKING(for -lconfdaemon)
            AC_LINK_IFELSE([
              AC_LANG_PROGRAM([#include <confdaemon/DaemonUtils.h>
	                       struct Daemon : public DaemonUtils::Base {
			         Daemon() : DaemonUtils::Base("","","","","","",
                                                              0,0,false) {}
                                 void SetupAuxServices() {}
				 void MakeCommandLine(const std::string&,
				      std::vector<std::string>&) {}
                               };],
                              [Daemon d()])], 
                              [have_libconfdaemon=yes])
            AC_MSG_RESULT($have_libconfdaemon)
    
            if test $have_confdaemon_daemonutils_h -gt 0    && \
                test "x$have_libconfdaemon"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_CONFDAEMON)
            else 
                with_confdaemon=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
    	CPPFLAGS=$save_CPPFLAGS
    	LDFLAGS=$save_LDFLAGS
        fi
    
        AC_MSG_CHECKING(where the ConfDaemon documentation is installed)
        if test "x$confdaemon_url" = "x" && \
    	test ! "x$CONFDAEMON_PREFIX" = "x" ; then 
           CONFDAEMON_URL=${CONFDAEMON_PREFIX}/share/doc/confdaemon
        else 
    	CONFDAEMON_URL=$confdaemon_url
        fi	
        AC_MSG_RESULT($CONFDAEMON_URL)
    fi
   
    if test "x$with_confdaemon" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(CONFDAEMON_URL)
    AC_SUBST(CONFDAEMON_PREFIX)
    AC_SUBST(CONFDAEMON_BINDIR)
    AC_SUBST(CONFDAEMON_DATADIR)
    AC_SUBST(CONFDAEMON_CPPFLAGS)
    AC_SUBST(CONFDAEMON_INCLUDEDIR)
    AC_SUBST(CONFDAEMON_LDFLAGS)
    AC_SUBST(CONFDAEMON_LIBDIR)
    AC_SUBST(CONFDAEMON_LIBS)
    AC_SUBST(CONFDAEMON_LTLIBS)
    AC_SUBST(CONFDAEMON_LTLDFLAGS)
])
dnl
dnl EOF
dnl 
