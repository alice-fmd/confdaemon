dnl __________________________________________________________________
dnl
dnl AC_ALTROCC([ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ALTROCC],
[
    AC_ARG_WITH([altrocc], 
    	    [AC_HELP_STRING([--with-altrocc],
    	                    [Prefix of ALTROCC installation])])
    save_LDFLAGS=$LDFLAGS
    save_CPPFLAGS=$CPPFLAGS
    if test ! "x$with_altrocc" = "x" && test ! "x$with_altrocc" = "xno" ; then 
       LDFLAGS="$LDFLAGS -L$with_altrocc/lib/altrocc" 
       CPPFLAGS="$CPPFLAGS -I$with_altrocc/include"
    else
       with_altrocc=
    fi
    have_altrocc_compiler_h=0
    AH_TEMPLATE(HAVE_ALTROCC_COMPILER_H, [Whether we have ALTROCC header])
    AC_CHECK_HEADER([altrocc/compiler.h], [have_altrocc_compiler_h=1])
    have_libaltrocc=0
    AC_CHECK_LIB([altrocc], [RCUC_compile], [have_libaltrocc=1])
    if test $have_libaltrocc -gt 0 && \
	test $have_altrocc_compiler_h -gt 0; then 
       AC_DEFINE(HAVE_ALTROCC_COMPILER_H)
       if test ! "x$with_altrocc" = "x" ; then 
          ALTROCCLDFLAGS="-L$with_altrocc/lib"
          ALTROCCCPPFLAGS="-I$with_altrocc/include"
       fi
       with_altrocc=yes
       ALTROCCLIB=-laltrocc
    else 
       with_altrocc=no
       ALTROCCLDFLAGS=
       ALTROCCPPFLAGS=
    fi				     
    LDFLAGS="$save_LDFLAGS"
    CPPFLAGS="$save_CPPFLAGS"

    if test "x$with_altrocc" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else 
        ifelse([$2], , :, [$2])
    fi
    AC_SUBST([ALTROCCLDFLAGS])
    AC_SUBST([ALTROCCCPPFLAGS])
    AC_SUBST([ALTROCCLIB])
])
dnl
dnl EOF
dnl 
