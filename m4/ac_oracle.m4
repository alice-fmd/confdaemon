dnl ------------------------------------------------------------------
dnl
dnl Check for Oracle
dnl AC_ORACLE([ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
dnl
AC_DEFUN([AC_ORACLE],
[
  AC_ARG_WITH([oracle-prefix],
	      [AC_HELP_STRING([--with-oracle-prefix=DIR],
		[Specify Oracle client installation prefix])])
  AC_ARG_WITH([oracle-libdir],
	      [AC_HELP_STRING([--with-oracle-libdir=DIR],
	        [Specify Oracle client library installation path])])
  AC_ARG_WITH([oracle-incdir],
	      [AC_HELP_STRING([--with-oracle-incdir=DIR],
		[Specify Oracle client header installation path])])

  AC_LANG_PUSH([C++])
  save_LDFLAGS=$LDFLAGS
  save_CPPFLAGS=$CPPFLAGS
  if test ! "x$with_oracle_prefix" = "x" && \
     test ! "x$with_oracle_prefix" = "xyes" ; then 
    with_oracle_libdir=$with_oracle_prefix
    with_oracle_incdir=$with_oracle_prefix/include
  fi
  if test ! "x$with_oracle_libdir" = "x" && \
     test ! "x$with_oracle_libdir" = "xyes" ; then 
    ORACLE_LDFLAGS="-L$with_oracle_libdir -Wl,-rpath,$with_oracle_libdir"
  fi
  if test ! "x$with_oracle_incdir" = "x" && \
     test ! "x$with_oracle_incdix" = "xyes" ; then 
    ORACLE_CPPFLAGS="-I$with_oracle_incdir"
  fi
  LDFLAGS="$LDFLAGS $ORACLE_LDFLAGS"
  CPPFLAGS="$CPPFLAGS $ORACLE_CPPFLAGS"
  have_oracle=yes
  save_LIBS="$LIBS"
  dnl AC_CHECK_LIB([nnz10],[main],[],[have_oracle=no], [-lssl])
  AC_CHECK_LIB([clntsh],[main],[],[have_oracle=no], [-lnnz10])
  AC_CHECK_LIB([ociei],[main],[],[have_oracle=no], [-lclntsh -lnnz10])
  AC_CHECK_LIB([occi],[main],[],[have_oracle=no], [-lclntsh -lnnz10 -lociei])
  AC_CHECK_HEADER([occi.h],[],[have_oracle=no])
  AC_MSG_CHECKING([whether to compile in Oracle support])
  AC_MSG_RESULT($have_oracle)
  AC_LANG_POP([C++])
  LDFLAGS="$save_LDFLAGS"
  CPPFLAGS="$save_CPPFLAGS"
  if test "x$have_oracle" = "xyes" ;  then 
    ORACLE_LIBS="-locci -lociei -lclntsh -lnnz10"
  else
    ORACLE_LDFLAGS=""
    ORACLE_CPPFLAGS=""
  fi
  LIBS="$save_LIBS"
  if test "x$have_oracle" = "xyes" ; then 
    ifelse([$1], , :, [$1])
  else 
    ifelse([$2], , :, [$2])
  fi
  AC_SUBST(ORACLE_LDFLAGS)
  AC_SUBST(ORACLE_CPPFLAGS)
  AC_SUBST(ORACLE_LIBS)
])
