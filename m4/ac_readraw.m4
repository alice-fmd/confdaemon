dnl __________________________________________________________________
dnl
dnl AC_READRAW([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_READRAW],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([readraw-prefix],
        [AC_HELP_STRING([--with-readraw-prefix],
		[Prefix where ReadRaw is installed])],
        readraw_prefix=$withval, readraw_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([readraw-url],
        [AC_HELP_STRING([--with-readraw-url],
		[Base URL where the ReadRaw dodumentation is installed])],
        readraw_url=$withval, readraw_url="")
    if test "x${READRAW_CONFIG+set}" != xset ; then 
        if test "x$readraw_prefix" != "x" ; then 
	    READRAW_CONFIG=$readraw_prefix/bin/readraw-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(READRAW_CONFIG, readraw-config, no)
    readraw_min_version=ifelse([$1], ,0.3,$1)
    
    # Message to user
    AC_MSG_CHECKING(for ReadRaw version >= $readraw_min_version)

    # Check if we got the script
    readraw_found=no    
    if test "x$READRAW_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       READRAW_CPPFLAGS=`$READRAW_CONFIG --cppflags`
       READRAW_INCLUDEDIR=`$READRAW_CONFIG --includedir`
       READRAW_LIBS=`$READRAW_CONFIG --libs`
       READRAW_LTLIBS=`$READRAW_CONFIG --ltlibs`
       READRAW_LIBDIR=`$READRAW_CONFIG --libdir`
       READRAW_LDFLAGS=`$READRAW_CONFIG --ldflags`
       READRAW_LTLDFLAGS=`$READRAW_CONFIG --ltldflags`
       READRAW_PREFIX=`$READRAW_CONFIG --prefix`
       
       # Check the version number is OK.
       readraw_version=`$READRAW_CONFIG -V` 
       readraw_vers=`echo $readraw_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       readraw_regu=`echo $readraw_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $readraw_vers -ge $readraw_regu ; then 
            readraw_found=yes
       fi
    fi
    AC_MSG_RESULT($readraw_found - is $readraw_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_READRAW_READER_H, 
                [Whether we have readraw/Reader.h header])
    AH_TEMPLATE(HAVE_READRAW, [Whether we have readraw])


    if test "x$readraw_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS $READRAW_LDFLAGS"
    	CPPFLAGS="$CPPFLAGS $READRAW_CPPFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_readraw_reader_h=0
        AC_CHECK_HEADER([readraw/Reader.h], [have_readraw_reader_h=1])

        # Check the library. 
        have_libreadraw=no
        AC_MSG_CHECKING(for -lreadraw)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <readraw/Reader.h>],
                        [unsigned long w; ReadRaw::Reader::SwapBytes(w)])], 
                        [have_libreadraw=yes])
        AC_MSG_RESULT($have_libreadraw)

        if test $have_readraw_reader_h -gt 0    && \
            test "x$have_libreadraw"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_READRAW_READER_H)
            AC_DEFINE(HAVE_READRAW)
        else 
            readraw_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
	CPPFLAGS=$save_CPPFLAGS
	LDFLAGS=$save_LDFLAGS
    fi

    AC_MSG_CHECKING(where the ReadRaw documentation is installed)
    if test "x$readraw_url" = "x" && \
	test ! "x$READRAW_PREFIX" = "x" ; then 
       READRAW_URL=${READRAW_PREFIX}/share/doc/readraw/html
    else 
	READRAW_URL=$readraw_url
    fi	
    AC_MSG_RESULT($READRAW_URL)
   
    if test "x$readraw_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(READRAW_URL)
    AC_SUBST(READRAW_PREFIX)
    AC_SUBST(READRAW_CPPFLAGS)
    AC_SUBST(READRAW_INCLUDEDIR)
    AC_SUBST(READRAW_LDFLAGS)
    AC_SUBST(READRAW_LIBDIR)
    AC_SUBST(READRAW_LIBS)
    AC_SUBST(READRAW_LTLIBS)
    AC_SUBST(READRAW_LTLDFLAGS)
])
dnl
dnl EOF
dnl 