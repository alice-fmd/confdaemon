dnl -*- mode: autoconf -*- 
dnl
dnl $Id: configure.ac,v 1.3 2012-03-06 09:39:14 cholm Exp $
dnl
dnl  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_INIT([ConfDaemon], [1.3], [cholm@nbi.dk], confdaemon, 
        [http://fmd.nbi.dk/fmd/fee/software.html])
AC_PREREQ(2.53)
AC_COPYRIGHT(GNU Lesser General Public License)
AC_REVISION($Revision: 1.3 $)
AC_CONFIG_SRCDIR(confdaemon/DaemonUtils.h)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_MACRO_DIR([m4])
AM_CONFIG_HEADER(config/config.h)
AC_PREFIX_DEFAULT(${HOME})
AM_INIT_AUTOMAKE([$PACKAGE_TARNAME], [$PACKAGE_VERSION])
sovers=2
AC_SUBST(sovers)
RELEASE=3
AC_SUBST(RELEASE)

dnl ------------------------------------------------------------------
dnl
dnl Check for C compiler 
dnl
AC_PROG_CC
AC_PROG_CXX

dnl
dnl Check for libtool
dnl
AC_PROG_LIBTOOL 

dnl ------------------------------------------------------------------
dnl
dnl Various flags
dnl
AC_DEBUG
AC_OPTIMIZATION
AC_PROFILING
AC_STRICT

dnl ------------------------------------------------------------------
dnl
dnl Check for DIM
dnl
AC_DIM(16.10, [], [AC_MSG_ERROR([I need DIM])])
extraldflags="$DIM_LDFLAGS"
AC_SUBST(extraldflags)

dnl ------------------------------------------------------------------
dnl
dnl Documentation
dnl
AC_ARG_VAR(DOXYGEN, The Documentation Generator)
AC_PATH_PROG(PERL, perl)
AC_PATH_PROG(DOXYGEN, doxygen)
AM_CONDITIONAL(HAVE_DOXYGEN, test ! "x$DOXYGEN" = "x")
DOC_KEYWORDS=""
HAVE_DOT=NO
DOT_PATH=
AC_PATH_PROG(DOT, dot)
if ! test "x$DOT" = "x" ; then
   HAVE_DOT=YES
   DOT_PATH=`dirname $DOT`
fi
AC_SUBST([DOC_KEYWORDS])
AC_SUBST([HAVE_DOT])
AC_SUBST([DOT_PATH])

dnl ------------------------------------------------------------------
dnl
dnl Some extra directory definitions 
dnl
AC_MSG_CHECKING(for system configuration directory)
initdir="\${sysconfdir}/init.d"
if test -f /etc/redhat-release ; then 
  initconfdir="\${sysconfdir}/sysconfig" 
else 
  initconfdir="\${sysconfdir}/default"
fi
piddir="\${localstatedir}/run"
logdir="\${localstatedir}/log"
AC_MSG_RESULT($initconfdir)
AC_SUBST(initdir)
AC_SUBST(initconfdir)
AC_SUBST(piddir)
AC_SUBST(logdir)

dnl ------------------------------------------------------------------
AC_CONFIG_FILES([Makefile 
	         support/confdaemon.spec
	         support/Makefile
		 support/libconfdaemon.pc
		 support/confdaemon-config
		 support/confdaemon_write_init
	         confdaemon/Makefile])
AC_OUTPUT

dnl
dnl EOF
dnl
